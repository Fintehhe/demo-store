//
//  KSLDetailViewController.h
//  Demo Store App
//
//  Created by Boris Polyakov on 7/2/14.
//  Copyright (c) 2014 KSL Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <QuickLook/QuickLook.h>

@class Item;

@interface KSLDetailViewController : UIViewController <MFMailComposeViewControllerDelegate, UIActionSheetDelegate, QLPreviewControllerDataSource>

@property (strong, nonatomic) Item *detailItem;

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *costLabel;
@property (nonatomic, strong) IBOutlet UITextView *desctiptionField;

@end
