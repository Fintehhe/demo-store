//
//  KSLAddItemViewController.m
//  Demo Store App
//
//  Created by Boris Polyakov on 7/3/14.
//  Copyright (c) 2014 KSL Technology. All rights reserved.
//

#import "KSLAddItemViewController.h"

static NSString *kDescriptionPlaceholder = @"Enter some description";

@interface KSLAddItemViewController ()

@end

@implementation KSLAddItemViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.costField.placeholder = @"Enter cost";
    
    self.nameField.placeholder = @"Enter product name";
    
    self.descriptionField.text = kDescriptionPlaceholder;
    self.descriptionField.textColor = [UIColor lightGrayColor];
    self.descriptionField.layer.borderWidth = 1.0f;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(close:)];
}

- (void)insertNewObject:(id)sender
{
    NSString *itemName = [self.nameField.text isEqualToString:@""] ? @"Some default name" : self.nameField.text;

    [[KSLDataBaseManager sharedInstance] insertItemWithParameters:@{
                                                                    @"itemName" : itemName,
                                                                    @"itemCost" : @(self.costField.text.floatValue),
                                                                    @"itemDescription" : self.descriptionField.text}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:kDescriptionPlaceholder]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = kDescriptionPlaceholder;
        textView.textColor = [UIColor lightGrayColor];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:self.nameField]) {
        return YES;
    }
    else {
        if ([string isEqualToString:@""]) {
            return YES;
        }
        NSError *error;
        NSRegularExpression *regEx = [NSRegularExpression regularExpressionWithPattern:@"^\\d{0,}$" options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger nmat = [regEx numberOfMatchesInString:string options:NSMatchingReportCompletion range:NSMakeRange(0, string.length)];
        if (nmat == string.length) {
            return YES;
        }
        else {
            return NO;
        }
        
    }
}

@end
