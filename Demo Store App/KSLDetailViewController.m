//
//  KSLDetailViewController.m
//  Demo Store App
//
//  Created by Boris Polyakov on 7/2/14.
//  Copyright (c) 2014 KSL Technology. All rights reserved.
//

#import "KSLDetailViewController.h"
#import "Item.h"

#define kDefaultPageHeight 792
#define kDefaultPageWidth  612
#define kMargin 10
#define kLogoWidth 274
#define kLogoHeight 26

@interface KSLDetailViewController ()

@property (nonatomic, strong) NSString *pdfFilePath;

- (void)configureView;
@end

@implementation KSLDetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(Item *)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.nameLabel.text = self.detailItem.itemName;
        self.costLabel.text = [NSString stringWithFormat:@"%.2f $", self.detailItem.itemCost.floatValue];
        self.desctiptionField.text = self.detailItem.itemDescription;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
    
}

- (IBAction)buyItem:(UIButton *)sender
{
    [[KSLDataBaseManager sharedInstance] buyItem:self.detailItem];
}

- (IBAction)share:(id)sender
{
    NSString* path = NSTemporaryDirectory();
    self.pdfFilePath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%f.pdf", [[NSDate date] timeIntervalSince1970] ]];
    
    UIGraphicsBeginPDFContextToFile(self.pdfFilePath, CGRectZero, nil);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, kDefaultPageWidth, kDefaultPageHeight), nil);
    
    UIImage *im = [UIImage imageNamed:@"logo.png"];
    CGFloat curY = kMargin;
    [im drawInRect:CGRectMake(kMargin, curY, kLogoWidth, kLogoHeight)];
    curY += kLogoHeight + kMargin;
    
    CGContextSetStrokeColorWithColor(context, [[UIColor blueColor] CGColor]);
    CGContextMoveToPoint(context, kMargin, curY);
    CGContextAddLineToPoint(context, kDefaultPageWidth - kMargin, curY);
    CGContextStrokePath(context);
    curY += kMargin;

    [self.view drawViewHierarchyInRect:CGRectMake(kMargin, curY, kDefaultPageWidth - kMargin * 2, kDefaultPageHeight - curY - kMargin) afterScreenUpdates:NO];
  
    UIGraphicsEndPDFContext();
    
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Would you like to preview or email this PDF?"
                                                              delegate:self
                                                     cancelButtonTitle:@"Cancel"
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:@"Preview", @"Email", nil];
    [actionSheet showInView:self.view];
}

#pragma mark - MFMailComposerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        // present a preview of this PDF File.
        QLPreviewController* preview = [[QLPreviewController alloc] init];
        preview.dataSource = self;
        [self presentViewController:preview animated:YES completion:nil];
        
    }
    else if(buttonIndex == 1) {
        // email the PDF File.
        MFMailComposeViewController* mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        [mailComposer addAttachmentData:[NSData dataWithContentsOfFile:self.pdfFilePath]
                               mimeType:@"application/pdf" fileName:@"report.pdf"];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
    }
    
}

#pragma mark - QLPreviewControllerDataSource
- (NSInteger) numberOfPreviewItemsInPreviewController: (QLPreviewController *) controller
{
    return 1;
}

- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    return [NSURL fileURLWithPath:self.pdfFilePath];
}


@end
