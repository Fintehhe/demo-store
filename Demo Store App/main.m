//
//  main.m
//  Demo Store App
//
//  Created by Boris Polyakov on 7/2/14.
//  Copyright (c) 2014 KSL Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KSLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KSLAppDelegate class]));
    }
}
