//
//  Item.h
//  Demo Store App
//
//  Created by Boris Polyakov on 7/2/14.
//  Copyright (c) 2014 KSL Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Item : NSManagedObject

@property (nonatomic, retain) NSString * itemDescription;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSNumber * itemCost;

@end
