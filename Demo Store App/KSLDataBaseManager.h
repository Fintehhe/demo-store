//
//  KSLDataBaseManager.h
//  Demo Store App
//
//  Created by Boris Polyakov on 7/2/14.
//  Copyright (c) 2014 KSL Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Item;

@interface KSLDataBaseManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (instancetype)sharedInstance;
- (void)insertItemWithParameters:(NSDictionary *)parameters;

- (void)buyItem:(Item *)item;

@end
