//
//  KSLAddItemViewController.h
//  Demo Store App
//
//  Created by Boris Polyakov on 7/3/14.
//  Copyright (c) 2014 KSL Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSLAddItemViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, strong) IBOutlet UITextField *nameField;
@property (nonatomic, strong) IBOutlet UITextField *costField;
@property (nonatomic, strong) IBOutlet UITextView *descriptionField;

@end
