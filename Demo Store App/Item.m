//
//  Item.m
//  Demo Store App
//
//  Created by Boris Polyakov on 7/2/14.
//  Copyright (c) 2014 KSL Technology. All rights reserved.
//

#import "Item.h"


@implementation Item

@dynamic itemDescription;
@dynamic itemName;
@dynamic itemCost;

@end
